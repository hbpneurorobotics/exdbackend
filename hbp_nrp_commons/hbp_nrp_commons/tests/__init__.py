"""
Unit tests for common functionality
"""

__author__ = 'Georg Hinkel'


from hbp_nrp_commons import python_version_major

builtins_str = "__builtin__" if python_version_major < 3 else "builtins"