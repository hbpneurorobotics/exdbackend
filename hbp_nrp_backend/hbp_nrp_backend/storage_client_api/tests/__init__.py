from hbp_nrp_backend.storage_client_api import python_version_major

builtins_str = "__builtin__" if python_version_major < 3 else "builtins"