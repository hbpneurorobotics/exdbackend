'''setup.py'''
from setuptools import setup

import hbp_nrp_backend

reqs_file = './requirements.txt'
install_reqs = list(val.strip() for val in open(reqs_file))
reqs = install_reqs

config = {
    'description': 'Experiment Backend for HBP SP10',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics-support@humanbrainproject.eu',
    'version': hbp_nrp_backend.__version__,
    'install_requires': reqs,
    'packages': ['hbp_nrp_backend',
                 'hbp_nrp_backend.rest_server',
                 'hbp_nrp_backend.storage_client_api',
                 'hbp_nrp_backend.simulation_control',
                 'hbp_nrp_backend.cle_interface'],
    'classifiers': ['Programming Language :: Python :: 3'],
    'scripts': [],
    'name': 'hbp_nrp_backend',
    'include_package_data': True,
}

setup(**config)
