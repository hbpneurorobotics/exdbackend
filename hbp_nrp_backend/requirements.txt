hbp-nrp-commons
# other requirements
lxml==4.6.5
PyYAML==5.4
rospkg==1.2.10
catkin_pkg==0.4.23
pyxb==1.2.6
Flask-Script==2.0.5
Flask-Migrate==1.6.0
psycopg2==2.8.6
timeout-decorator==0.4.1
urllib3[secure]==1.26.5
ptvsd==4.3.2
requests
cryptography>=2.8
backports.functools_lru_cache==1.5; python_version < "3.0"
future

# NRRPLT-8660
Flask==2.0.1
