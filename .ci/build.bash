#!/bin/bash

set -e
set -x

whoami
env | sort
pwd


if [ -z "${HBP}" ]; then
    echo "USAGE: The HBP variable not specified"
    exit 1
fi

USER_SCRIPTS_DIR=${USER_SCRIPTS_DIR:-"user-scripts"}
EXP_CONTROL_DIR=${EXP_CONTROL_DIR:-"ExperimentControl"}
CLE_DIR=${CLE_DIR:-"CLE"}
BRAIN_SIMULATION_DIR=${BRAIN_SIMULATION_DIR:-"BrainSimulation"}
EXDBACKEND_DIR=${EXDBACKEND_DIR:-"ExDBackend"}

export PYTHONPATH=

source "${HBP}/${USER_SCRIPTS_DIR}/nrp_variables"

# Configure build has to be placed before make devinstall

export NRP_INSTALL_MODE=dev

cd "${HBP}/${EXDBACKEND_DIR}"

# Concatenate all build requirements, ensure newline in between
(echo; cat "${HBP}/${EXP_CONTROL_DIR}/hbp_nrp_excontrol/requirements.txt") >> hbp_nrp_backend/requirements.txt
(echo; cat "${HBP}/${CLE_DIR}/hbp_nrp_cle/requirements.txt") >> hbp_nrp_backend/requirements.txt

# Checkout config.ini.sample from user-scripts
cp "${HBP}/${USER_SCRIPTS_DIR}/config_files/CLE/config.ini.sample" "${HBP}/${CLE_DIR}/hbp_nrp_cle/hbp_nrp_cle/config.ini"

# Obtain schemas
# This variable is needed for common_makefile during schemas generation
export DEFAULT_CO_BRANCH=${DEFAULT_BRANCH}
export TOPIC_BRANCH

cd "${HBP}"
# Run tests
export IGNORE_LINT='platform_venv|hbp_nrp_commons/hbp_nrp_commons/generated|hbp-flask-restful-swagger-master|migrations|nest|ci_download_directory'
# build CLE before ExDBackend
# verify_base-ci fails on dependencies mismatch, but ignores linter errors, which are cought by Jenkins afterwards
rm -rf build_env
virtualenv build_env \
    && . build_env/bin/activate \
    && cd "${HBP}/${CLE_DIR}" \
    && make --always-make devinstall \
    && cd "${HBP}/${EXDBACKEND_DIR}" \
    && make --always-make schemas-install \
    && make --always-make verify_base-ci
