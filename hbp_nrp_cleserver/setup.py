'''setup.py'''
import os

from setuptools import setup

import hbp_nrp_cleserver


def parse_reqs(reqs_file):
    ''' parse the requirements '''
    install_reqs = list(val.strip() for val in open(reqs_file))
    reqs = install_reqs
    return reqs


BASEDIR = os.path.dirname(os.path.abspath(__file__))
REQS = parse_reqs(os.path.join(BASEDIR, 'requirements.txt'))

EXTRA_REQS_PREFIX = 'requirements_'
EXTRA_REQS = {}
for file_name in os.listdir(BASEDIR):
    if not file_name.startswith(EXTRA_REQS_PREFIX):
        continue
    base_name = os.path.basename(file_name)
    (extra, _) = os.path.splitext(base_name)
    extra = extra[len(EXTRA_REQS_PREFIX):]
    EXTRA_REQS[extra] = parse_reqs(file_name)

config = {
    'description': 'CLE Simulation Factory for HBP SP10',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics-support@humanbrainproject.eu',
    'version': hbp_nrp_cleserver.__version__,
    'install_requires': REQS,
    'extras_require': EXTRA_REQS,
    'packages': ['hbp_nrp_cleserver',
                 'hbp_nrp_cleserver.bibi_config',
                 'hbp_nrp_cleserver.server'],
    'package_data': {
        'hbp_nrp_cleserver.bibi_config': ['tf_template.pyt']
    },
    'classifiers': ['Programming Language :: Python :: 3'],
    'scripts': [],
    'name': 'hbp-nrp-cleserver',
    'include_package_data': True,
}

setup(**config)
