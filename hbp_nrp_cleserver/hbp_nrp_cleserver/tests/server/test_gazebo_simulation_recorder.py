# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Test for ROS wrapper for Gazebo simulation recorder plugin.
"""

from unittest.mock import Mock, call, patch
import unittest

from hbp_nrp_cleserver.server.GazeboSimulationRecorder import GazeboSimulationRecorder

from cle_ros_msgs import srv
from std_srvs.srv import Trigger
from gazebo_ros_replay_plugins.srv import Start

import rospy


class TestGazeboSimulationRecorder(unittest.TestCase):

    def setUp(self):
        rospy.Service = Mock()
        self.rospy_service_mock = patch("rospy.Service").start()
        self.rospy_service_proxy_mock = patch("rospy.ServiceProxy").start()
        self.rospy_service_mock.shutdown = Mock()

        self.recorder = GazeboSimulationRecorder(0)

    def tearDown(self):
        self.rospy_service_mock.stop()
        self.rospy_service_proxy_mock.stop()

    def test_init(self):
        self.rospy_service_mock.assert_called_with('/ros_cle_simulation/0/simulation_recorder',
                                                   srv.SimulationRecorder,
                                                   self.recorder._GazeboSimulationRecorder__command)

        self.rospy_service_proxy_mock.assert_has_calls([call('/gazebo/recording/start', Start),
                                                        call('/gazebo/recording/stop', Trigger),
                                                        call('/gazebo/recording/cancel', Trigger),
                                                        call('/gazebo/recording/cleanup', Trigger),
                                                        call('/gazebo/recording/get_recording',
                                                             Trigger)])

    def test_shutdown(self):
        self.recorder.shutdown()

        # ensure the shutdowns are properly called
        self.assertTrue(
            self.recorder._GazeboSimulationRecorder__service_simulation_recorder.shutdown.called)

        self.recorder._GazeboSimulationRecorder__recorder_cleanup.assert_called_once()

    def test_command(self):
        request_class = srv.SimulationRecorderRequest

        # request types defined in srv.SimulationRecorderRequest
        req_to_cmd_map = {request_class.STATE: "state",
                          request_class.START: "start",
                          request_class.STOP: "stop",
                          request_class.CANCEL: "cancel",
                          request_class.RESET: "cleanup"}

        for request_type in req_to_cmd_map:
            self._test_command(req_to_cmd_map, request_type)

        # invalid call
        resp = self.recorder._GazeboSimulationRecorder__command(request_class('foo'))

        self.assertEqual(False, resp.value)
        self.assertEqual('Invalid Simulation Recorder command: foo', resp.message)

    def _test_command(self, req_to_cmd_map, request_type):
        recorder_command = self.recorder._GazeboSimulationRecorder__command

        request = srv.SimulationRecorderRequest(request_type)

        request.request_type = request_type
        command_clbk = "_GazeboSimulationRecorder__recorder_%s" % req_to_cmd_map[request_type]

        with patch.object(self.recorder, command_clbk):
            recorder_command(request)
            getattr(self.recorder, command_clbk).assert_called_once()


if __name__ == '__main__':
    unittest.main()
